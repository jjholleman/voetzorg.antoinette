<?php
/**
 * Template Name: Homepagina
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['recent_posts'] = get_posts();
$context['post'] = $post;
$context['homepage'] = [
	'intro_title' => get_field('intro_title'),
	'intro_subtitle' => get_field('intro_subtitle'),
	'intro_appointment_btn_label' => get_field('intro_appointment_btn_label')
];

Timber::render( array( 'custom/page-home.twig', 'page.twig' ), $context );