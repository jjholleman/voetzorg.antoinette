<?php
/**
 * Template Name: Productpagina
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['recent_posts'] = get_posts();
$context['post'] = $post;
$context['products'] = get_field('products');

Timber::render( array( 'custom/page-producten.twig', 'page.twig' ), $context );