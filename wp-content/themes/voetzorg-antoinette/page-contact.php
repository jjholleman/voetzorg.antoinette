<?php
/**
 * Template Name: Contactpagina
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

Timber::render( array( 'custom/page-contact.twig', 'page.twig' ), $context );