let mix = require('laravel-mix');

mix.sass('src/styles/main.scss', '/style.css')
    .setResourceRoot('public')
    .options({
        processCssUrls: false,
    })